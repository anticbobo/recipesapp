import { Directive, HostBinding, HostListener, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  @HostBinding('class.show') isOpen: boolean = false;
  dropdown: string = '';

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener('click') openDropdown(eventData: Event) {
    this.isOpen = !this.isOpen;
    const div = this.el.nativeElement.querySelector('div');
    if(this.dropdown === '') {
      this.renderer.addClass(div, 'show');
      this.dropdown = 'show';
    }
    else {
      this.renderer.removeClass(div, 'show');
      this.dropdown = '';
    }
  }

}
