import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.scss']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  shoppingForm: FormGroup;
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;

  constructor(private slService: ShoppingService) { }

  ngOnInit() {
    this.shoppingForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.minLength(3)]),
      'amount': new FormControl(null, [Validators.required, Validators.pattern("^[1-9]+[0-9]*$")])
    })
    this.subscription = this.slService.editingIngredient.subscribe((index: number) => {
      this.editedItemIndex = index;
      this.editMode = true;
      this.editedItem = this.slService.getIngredient(index);
      this.shoppingForm.setValue({
        'name': this.editedItem.name,
        'amount': this.editedItem.amount
      })
    });
  }

  addToList() {
    const name = this.shoppingForm.get('name').value;
    const amount = this.shoppingForm.get('amount').value;
    const ingredient = new Ingredient(name, amount);
    if(this.editMode) {
      this.slService.updateIngredient(this.editedItemIndex, ingredient);
    }
    else {
      this.slService.addIngredient(ingredient);
    }
    this.clearInputs();
  }

  clearInputs() {
    this.shoppingForm.reset();
    this.editMode = false;
  }

  deleteFromList() {
    this.clearInputs();
    this.slService.deleteIngredient(this.editedItemIndex);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    this.subscription.unsubscribe();
  }

}
