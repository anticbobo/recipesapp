import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { ShoppingService } from './shopping.service';
import { Recipe } from '../recipes/recipe-list/recipe.model';
import { Ingredient } from '../shared/ingredient.model';

@Injectable()
export class RecipeService {

    selectedRecipe = new Subject<Recipe>();
    recipeChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [
        new Recipe('Sarma', 'Traditional Macedonian dish', 'https://kurir.mk/wp-content/uploads/2018/12/sarma.jpg', [
            new Ingredient('Kupus', 5),
            new Ingredient('Meleno meso', 1),
            new Ingredient('Rebro', 2)
        ]),
        new Recipe('Grav', 'Good for your gastrointestines', 'http://balkon3.com/mk/wp-content/uploads/2012/08/baked-beans-in-pot.jpg', [
            new Ingredient('Grav', 4),
            new Ingredient('Rebro', 3)
        ])
    ]

    constructor(private slService: ShoppingService) {}

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addToShopList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipeChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipeChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
    }

}