import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';

export class ShoppingService {

    ingredientsChanged = new Subject<Ingredient[]>();
    editingIngredient = new Subject<number>();

    private ingredients: Ingredient[] = [
        new Ingredient('Kupus', 1),
        new Ingredient('Mesko', 2)
    ];

    getIngredients() {
        return this.ingredients.slice();
    }

    getIngredient(i: number) {
        return this.ingredients[i];
    }

    addIngredient(ingredient: Ingredient) {
        this.ingredients.push(ingredient);
        this.ingredientsChanged.next(this.getIngredients());
    }

    addIngredients(ingredients: Ingredient[]) {
        ingredients.forEach(ingredient => {
            this.ingredients.push(ingredient);
        })
        this.ingredientsChanged.next(this.ingredients.slice())
    }

    updateIngredient(index: number, newIngredient: Ingredient) {
        this.ingredients[index] = newIngredient;
        this.ingredientsChanged.next(this.ingredients.slice()); 
    }
    
    deleteIngredient(index: number) {
        this.ingredients.splice(index, 1);
        this.ingredientsChanged.next(this.ingredients.slice());
    }

}