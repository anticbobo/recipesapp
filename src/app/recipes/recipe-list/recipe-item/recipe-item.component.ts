import { Component, OnInit, Input } from '@angular/core';

import { Recipe } from '../recipe.model';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.scss']
})
export class RecipeItemComponent implements OnInit {

  @Input() recipe: Recipe;
  @Input() index: number;
  ingredientCount: number;

  constructor() {
    
  }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.getIngredientCount();    
  }

  getIngredientCount() {
    const count = this.recipe.ingredients.length;
    this.ingredientCount = count;
  }


}
