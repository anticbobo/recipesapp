import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from '../recipe-list/recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss']
})
export class RecipeEditComponent implements OnInit {

  id: number;
  editMode = false;
  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
    })

    // to be implemented - delete recipe subscription to Subject
    this.initForm();
  }

  private initForm() {
    let recipeName = '';
    let imgPath = '';
    let description = '';
    let ingredients = new FormArray([]);

    if(this.editMode) {
      const recipe = this.recipeService.getRecipe(this.id);
      recipeName = recipe.name;
      imgPath = recipe.imgPath;
      description = recipe.description;
      if(recipe['ingredients']) {
        for(let ingredient of recipe.ingredients) {
          ingredients.push(
            new FormGroup({
              'name': new FormControl(ingredient.name, [Validators.required, Validators.minLength(3)]),
              'amount': new FormControl(ingredient.amount, [Validators.required, Validators.pattern("^[1-9]+[0-9]*$")])
            })
          );
        }
      }
    }
    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, [Validators.required, Validators.minLength(4)]),
      'description': new FormControl(description, [Validators.required, Validators.minLength(10)]),
      'imgPath': new FormControl(imgPath, Validators.required),
      'ingredients': ingredients
    })
  }

  getIngredientsControls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

  addIngredient() {
    const ingredientFields = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.minLength(3)]),
      'amount': new FormControl(null, [Validators.required, Validators.pattern("^[1-9]+[0-9]*$")])
    });
    (<FormArray>this.recipeForm.get('ingredients')).push(ingredientFields);
  }

  onSubmit() {
    /* let recipeValues = this.recipeForm.value;
    const newRecipe = new Recipe(
      recipeValues['name'], 
      recipeValues['description'], 
      recipeValues['imgPath'], 
      recipeValues['ingredients']); */
    if(this.editMode) {
      this.recipeService.updateRecipe(this.id, this.recipeForm.value);
    }
    else {
      this.recipeService.addRecipe(this.recipeForm.value);
    }
  }

}
